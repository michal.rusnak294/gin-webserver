package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	_ "gin-webserver/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

var appVersion, commit, buildTime string
var versions Versions
var censors = make(map[string]bool)
var mu sync.Mutex

// @title     Golang Bookstore API
// @version         1.0.0
// @description     A book management service API in Go using Gin framework.
// @contact.name   Viliam L
// @contact.url    https://twitter.com/
// @host      localhost:8080
// @BasePath  /api/v1
func main() {
	versions.Version = appVersion
	versions.Commit = commit
	versions.BuildTime = buildTime

	r := gin.Default()
	r.GET("/api/v1/version", version)
	r.GET("/api/v1/authors", getAuthors)
	r.GET("/api/v1/books", getBooks)
	r.POST("/api/v1/censors", enpSetCensors)

	r.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	srv := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	log.Print("Server Started")

	<-done
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second) // waits 20 sec before exit
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}

	log.Print("Server Exited Properly")

}

// version             godoc
// @Summary      Get app version
// @Description  Returns the version, commit hash and build time.
// @Tags         version
// @Produce      json
// @Success      200  {array}  Versions
// @Router       /version [get]
func version(c *gin.Context) {
	time.Sleep(10 * time.Second)
	c.IndentedJSON(http.StatusOK, Versions{Version: versions.Version, Commit: versions.Commit, BuildTime: versions.BuildTime})
}

// getAuthors             godoc
// @Summary      Get authors of book
// @Description  Returns the authors of specified book.
// @Tags         book
// @Produce      json
// @Success      200  {array}  RespBooks
// @Failure      400  {object} ErrorResponse
// @Failure      500  {object} ErrorResponse
// @Router       /authors [get]
// @Param        book query string false "ISBN Book"
func getAuthors(c *gin.Context) {
	bookAuthors := Book{}
	name := Name{}

	book := c.Query("book")
	fmt.Println("book:", book)
	if book == "" {
		c.IndentedJSON(http.StatusBadRequest, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}

	// TODO: ISBN regex check

	url := "https://openlibrary.org/isbn/" + book + ".json"

	body, err := apiCall(url)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	err = json.Unmarshal(body, &bookAuthors)
	if err != nil {
		fmt.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	respA := make([]RespAuthor, len(bookAuthors.Authors))

	for index := range bookAuthors.Authors {
		url = "https://openlibrary.org" + bookAuthors.Authors[index].Key + ".json"
		body, err = apiCall(url)
		if err != nil {
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
			return
		}
		err := json.Unmarshal(body, &name)
		if err != nil {
			fmt.Println("error:", err)
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
			return
		}

		bookAuthors.Authors[index].Key = strings.ReplaceAll(bookAuthors.Authors[index].Key, "/authors/", "")

		respA[index].Name = name.Name
		respA[index].Key = bookAuthors.Authors[index].Key
	}

	c.IndentedJSON(http.StatusOK, respA)

}

func getBooks(c *gin.Context) {
	authorBooks := Entries{}

	author := c.Query("author")
	if author == "" {
		c.IndentedJSON(http.StatusBadRequest, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}
	if isCensored(author) {
		c.IndentedJSON(http.StatusForbidden, ErrorResponse{Error: "this author is censored and his/her books will not be shown"})
		return
	}

	url := "https://openlibrary.org/authors/" + author + "/works.json?limit=10"

	body, err := apiCall(url)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		fmt.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	c.IndentedJSON(http.StatusOK, respB)
}

func enpSetCensors(c *gin.Context) {

	reqCensors := []string{}

	err := c.BindJSON(&reqCensors)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}
	setCensors(reqCensors)
	c.Status(http.StatusOK)

}

func setCensors(cens []string) {
	mu.Lock()
	for _, c := range cens {
		censors[c] = true
	}
	mu.Unlock()
}

func isCensored(author string) bool {
	mu.Lock()
	defer mu.Unlock()
	return censors[author]
}

func apiCall(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	// !!! nacitava cely []byte do pamate, lepsie bufio.NewReader()
	body, err := ioutil.ReadAll(resp.Body)

	return body, err
}

// merge request try
// curl localhost:8080/api/v1/books?author=OL1394244A

